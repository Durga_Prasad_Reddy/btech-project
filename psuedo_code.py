# Phase 1 Psuedo Code

rows[] array consists of all student details and is sorted according to high CGPA first
branch_info_rows[] consists of details of each branch (Name, Existing strength, Sanctioned Strength)

while True:
	for each student ;
		for each of student's preference branch ;
			if currentPreference is not none and status is not yes:
				if current branch strength wont fall below floor value:
					if preference branch strength wont cross ceiling value:
						increment preference branch strength by 1;
						decrement current branch strength by 1;
						update status of his preference branch in his list of values to 'yes';
						update his current branch to that particular preference branch;
						update global status of his current branch to topper left;
						increment a flag value;
						break

					else:
						update status of his preference branch in his list of values to 'maximum strength of preference branch reached'

				elif global status of his current branch is 'highest scorer did not leave yet':
					if preference branch strength wont cross ceiling value:
						increment preference branch strength by 1;
						decrement current branch strength by 1;
						update his current branch to that particular preference branch;
						update global status of current branch as 'highest scorer left';
						incerement flag value;
						break

					else:
						update status of his preference branch in his list of values to 'maximum strength of preference branch reached'

				elif global status of preference branch contains 'got rejected' and it doesn't contain his name:
					update status of his preference branch in his list of values to global status of preference branch;

				# We are checking whether global status of preference branch contains his own name or not because if it contains his name
				# then it implies he was the highest GPA student who got rejected and reason for it is current strength bottomed out

				else:
					update status of his preference branch as 'minimum strength of current branch reached' in his list of values';
					if global status of preference branch is not 'highest scorer of this branch didnt leave yet':
						update global status of preference branch to currentStudent.name + 'was rejected';
					# the essence of this update is first time it gets into this else and from next on the loop goes into line 33

			else:
				break;

		if flag value didnt change:
			break from while loop;



# Psuedo code for Phase-2 deadlock resolve

create pendingRequestGraph;
# Each node in pending request graph is a branch and each edge is a student it carries a weight which is the student details
# It is a directed graph. An edge is drawn from current branch of student to all his reject preference branches. 
# His current branch in this case may not be his initial current branch it may be interim after Phase-1.

create resolvableRequestGraph;
# This is a subgraph of pendingRequestGraph