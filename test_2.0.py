import csv 
filename = "testCases.txt"
rows = []  
with open(filename, 'r') as csvfile: 
    csvreader = csv.reader(csvfile) 
    for row in csvreader: 
        rows.append(row) 
    print("Total no. of rows: %d"%(csvreader.line_num))
    no_of_students = csvreader.line_num  
 
print('\nThe records are:\n') 

for row in rows: 
    for col in row: 
        print("%10s"%col), 
    print('\n') 

dummy_list = ['Tinku','111501012','9.6','5189','civil','cse','ee','mech','none','none','none'] 

class Branch:
	def __init__(self,list2):
		self.branchName = list2[0]
		self.existingStrength = list2[1]
		self.sanctionedStrength = list2[2]

branch_info_rows = []

with open("branch_info.txt", 'r') as readFile:
	branch_info_reader = csv.reader(readFile)

	for branch_info_row in branch_info_reader:
		branch_info_rows.append(branch_info_row)

	print("Total no. of rows in branch info file is: %d"%(branch_info_reader.line_num))
	no_of_branches = branch_info_reader.line_num

print('\n The branch info records are : \n')

for branch_info_row in branch_info_rows:
	for branch_info_column in branch_info_row:
		print("%10s"%branch_info_column), 
	print('\n')

temporaryStrengthDictionary = {}

for i in range(0,4):
	branch1 = Branch(branch_info_rows[i])
	temporaryStrengthDictionary[branch1.branchName] = branch1.existingStrength
	print(branch1.branchName)

print(temporaryStrengthDictionary['cse'])
statusBranchDictionary = {}

for i in range(0,4):
	branch1 = Branch(branch_info_rows[i])
	statusBranchDictionary[branch1.branchName] = 'topper of branch did not leave yet'

print(statusBranchDictionary)
leastStrengthBranches = []

class Student:
	def __init__(self, list1):
		self.name = list1[0]
		self.roll_no = list1[1]
		self.GPA = list1[2]
		self.JEE_rank = list1[3]
		self.current_branch = list1[4]
		self.preference_branch = [list1[5], list1[6], list1[7]]
		self.status_of_branch = [list1[8], list1[9], list1[10]]

	def writeBack(self, list1):
		list1[0] = self.name
		list1[1] = self.roll_no
		list1[2] = self.GPA
		list1[3] = self.JEE_rank
		list1[4] = self.current_branch
		list1[5] = self.preference_branch[0]
		list1[6] = self.preference_branch[1]
		list1[7] = self.preference_branch[2]
		list1[8] = self.status_of_branch[0]
		list1[9] = self.status_of_branch[1]
		list1[10] = self.status_of_branch[2]

def sortRank(val):
	return val[3]

rows.sort(key = sortRank)
print("after rank sorting")
print(rows)

def sortGPA(val):
	return val[2]

rows.sort(key = sortGPA, reverse = True)
print("after sorting ")
print(rows)

flag = -100
anotherFlag = -100
tempString = 'left'

while True:
	anotherFlag = flag
		# looping over all students
	for temp1 in range(0,no_of_students):
		currentStudent = Student(rows[temp1])
		print(currentStudent.name)			
		print(statusBranchDictionary['cse'])
			# loop to find out current branch in branch_info.txt and use the temp2 in further calculations
		for temp2 in range(0,no_of_branches):
			if currentStudent.current_branch == Branch(branch_info_rows[temp2]).branchName:
				break
			# looping over all preference branches 
		for temp3 in range(0,3):
			currentPreference = currentStudent.preference_branch[temp3]

			if currentPreference != 'none' and currentStudent.status_of_branch[temp3] != 'yes':
					# finding preference branch in branch_info.txt
				for temp4 in range(0,no_of_branches):
					if currentPreference == Branch(branch_info_rows[temp4]).branchName:
						break
					#check whether current branch existing strength has not fallen below 10%
				if int(temporaryStrengthDictionary[currentStudent.current_branch]) - 1 >= 0.9 * int(Branch(branch_info_rows[temp2]).existingStrength):
						#check whether preference branch strength has not gone up by 10% of sanctioned
					if int(temporaryStrengthDictionary[currentStudent.preference_branch[temp3]]) + 1 <= 1.1 * int(Branch(branch_info_rows[temp4]).sanctionedStrength):
						
						print("printing current student current branch")
						print(currentStudent.current_branch)
						print("old value ")
						print(statusBranchDictionary[currentStudent.current_branch])
						statusBranchDictionary[currentStudent.current_branch] = 'left'
						statusBranchDictionary[currentStudent.preference_branch[temp3]] = 'no problem'
						print("new change ")
						print(statusBranchDictionary[currentStudent.current_branch])
						currentStudent.status_of_branch[temp3] = 'yes'
						temporaryStrengthDictionary[currentStudent.current_branch] = int(temporaryStrengthDictionary[currentStudent.current_branch]) - 1
						temporaryStrengthDictionary[currentStudent.preference_branch[temp3]] = int(temporaryStrengthDictionary[currentStudent.preference_branch[temp3]]) + 1
						currentStudent.writeBack(rows[temp1])
						flag = flag + 1

						break

					else:
						currentStudent.status_of_branch[temp3] = 'maximum strength of preference branch reached'
						currentStudent.writeBack(rows[temp1])

				elif statusBranchDictionary[currentStudent.current_branch] == 'topper of branch did not leave yet':
						#check whether preference branch strength has not gone up by 10% of sanctioned
					if int(temporaryStrengthDictionary[currentStudent.preference_branch[temp3]]) + 1 <= 1.1 * int(Branch(branch_info_rows[temp4]).sanctionedStrength):
						
						currentStudent.status_of_branch[temp3] = 'yes'
						statusBranchDictionary[currentStudent.current_branch] = tempString
						temporaryStrengthDictionary[currentStudent.current_branch] = int(temporaryStrengthDictionary[currentStudent.current_branch]) - 1
						temporaryStrengthDictionary[currentStudent.preference_branch[temp3]] = int(temporaryStrengthDictionary[currentStudent.preference_branch[temp3]]) + 1
						currentStudent.writeBack(rows[temp1])
						flag = flag + 1
						break

					else:
						currentStudent.status_of_branch[temp3] = 'maximum strength of preference branch reached'
						currentStudent.writeBack(rows[temp1])
						# check this I have to edit try using % for checking if string has 'got rejected'
				elif statusBranchDictionary[currentStudent.preference_branch[temp3]].find('got rejected') != -1 and statusBranchDictionary[currentStudent.preference_branch[temp3]].find(currentStudent.name) == -1:
					currentStudent.status_of_branch[temp3] = statusBranchDictionary[currentStudent.preference_branch[temp3]]
					currentStudent.writeBack(rows[temp1])

					# from this student onwards everyone applying to that branch will be rejected
				else:
					currentStudent.status_of_branch[temp3] = 'minimum strength of current branch reached'
					if statusBranchDictionary[currentStudent.preference_branch[temp3]] == 'topper of branch did not leave yet':
						tempString = currentStudent.name + ' got rejected'

					else:
						statusBranchDictionary[currentStudent.preference_branch[temp3]] = currentStudent.name + ' got rejected'
					currentStudent.writeBack(rows[temp1])

			else:
				break

	print('after all students')
	print(statusBranchDictionary['civil'])	
	print(temporaryStrengthDictionary)
	print(statusBranchDictionary)

	if anotherFlag == flag:
		break
# this is wrong loop what if topper left and one guy joined the branch?
'''
	for temp5 in range(0,no_of_branches):
		tempBranchName = Branch(branch_info_rows[temp2]).branchName

		if temporaryStrengthDictionary[tempBranchName] == Branch(branch_info_rows[temp2]).existingStrength:
			statusBranchDictionary[tempBranchName] = 'topper of branch did not leave yet'

		else:
			statusBranchDictionary[tempBranchName] = 'left'
'''
	for j in range (0,4):
		searchBranch = Branch(branch_info_rows[j])

		if int(temporaryStrengthDictionary[searchBranch.branchName]) <= 0.9 * int(searchBranch.existingStrength):
			leastStrengthBranches.append(searchBranch.branchName)


print(leastStrengthBranches)
deadlockStudents = []

for i in range(0, no_of_students):

	currentBranchIsThere = 0
	preferenceBranchIsThere = 0
	dLockStudent = Student(rows[i])
	
	for j in range(0, len(leastStrengthBranches)):

		if dLockStudent.current_branch == leastStrengthBranches[j]:
			currentBranchIsThere = 1

		# check whether status of preference branch is minimum strength of current branch reached
		for k in range(0,3):
			if dLockStudent.preference_branch[k] == leastStrengthBranches[j]:
				preferenceBranchIsThere = 1

	if currentBranchIsThere == 1 and preferenceBranchIsThere == 1:
		deadlockStudents.append(rows[i])

print('deadlock students are')
print(deadlockStudents)

deadlockBranchDictionary = {}
for i in range(0,no_of_branches):
	currentBranch = Branch(branch_info_rows[i])
	deadlockBranchDictionary[currentBranch.branchName] = []


# 
for i in range(0,len(deadlockStudents)):
	checkDeadlockStudents = Student(deadlockStudents[i])

	for j in range(0,3):
		if checkDeadlockStudents.status_of_branch[j] == 'minimum strength of current branch reached' and checkDeadlockStudents.preference_branch[j] != 'none':
			deadlockBranchDictionary[checkDeadlockStudents.current_branch].append(checkDeadlockStudents.preference_branch[j])

print(deadlockBranchDictionary)

def dfs(graph, start, end):
    fringe = [(start, [])]
    while fringe:
        state, path = fringe.pop()
        if path and state == end:
            yield path
            continue
        for next_state in graph[state]:
            if next_state in path:
                continue
            fringe.append((next_state, path+[next_state]))

cycles = [[node]+path  for node in deadlockBranchDictionary for path in dfs(deadlockBranchDictionary, node, node)]            

print(cycles)

# put a while loop remove edges in graph each time a deadlock is resolved and re run the cycles part
# remove students from deadlockStudents list whose status has changed to yes
while len(cycles) != 0:
	for i in range(0,len(deadlockStudents)):
		checkDeadlockStudents = Student(deadlockStudents[i])
			
		for j in range(0,3):
			for k in range(0,3):
				if checkDeadlockStudents.status_of_branch[k] == 'minimum strength of current branch reached' and checkDeadlockStudents.preference_branch[k] != 'none':
					deadlockBranchDictionary[checkDeadlockStudents.current_branch].append(checkDeadlockStudents.preference_branch[k])

			if currentStudent.preference_branch[j] in cycles[0] and currentStudent.current_branch in cycles[0]:
				temporaryStrengthDictionary[checkDeadlockStudents.current_branch] = int(temporaryStrengthDictionary[checkDeadlockStudents.current_branch]) - 1
				temporaryStrengthDictionary[checkDeadlockStudents.preference_branch[j]] = int(temporaryStrengthDictionary[checkDeadlockStudents.preference_branch[j]]) + 1
				checkDeadlockStudents.status_of_branch[j] = 'deadlock resolved'
				checkDeadlockStudents.writeBack(deadlockStudents[i])
				print("printing preference branch of deadlock student ")
				print(checkDeadlockStudents.preference_branch[j])
				# now remove the edge between current branch and preference branch
				deadlockBranchDictionary[checkDeadlockStudents.current_branch].remove(checkDeadlockStudents.preference_branch[j])

	cycles = [[node]+path  for node in deadlockBranchDictionary for path in dfs(deadlockBranchDictionary, node, node)]


print('after resolving deadlock')
print(deadlockStudents)


for i in range(0,no_of_students):
	student1 = Student(rows[i])

	for j in range(0,3):
		if student1.status_of_branch[j] == 'yes':
			if j == 0:
				student1.status_of_branch[1] = 'already got a preference'
				student1.status_of_branch[2] = 'already got a preference'
				student1.writeBack(rows[i])
								
			if j == 1:
				student1.status_of_branch[2] = 'already got a preference'
				student1.writeBack(rows[i])


with open("branch_info.txt",'w') as branchInfoWriteFiles:
	branchWriter = csv.writer(branchInfoWriteFiles)
	branchWriter.writerows(branch_info_rows)

with open(filename, 'w') as writeFiles:
	writer = csv.writer(writeFiles)
	writer.writerows(rows)


print(temporaryStrengthDictionary)

print(statusBranchDictionary)

writeToCSV = open("1.csv",'w')

with open("1.csv", "w") as writeToCSV:
	writeToCSV.write("Name,Roll No,GPA,JEE Rank,Current branch,Preference 1,Preference 2,Preference 3,Status of P1,Status of P2,Status of P3")
	writeToCSV.write("\n")
	writer = csv.writer(writeToCSV)
	writer.writerows(rows)
'''
# write this at start of the file
'''