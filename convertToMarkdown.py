
# importing csv module 
import csv 
  
# csv file name 
filename = "testCases.txt"
  
# initializing the titles and rows list 

rows = [] 
  
# reading csv file 
with open(filename, 'r') as csvfile: 
    # creating a csv reader object 
    csvreader = csv.reader(csvfile) 
      
    # extracting field names through first row 
 
  
    # extracting each data row one by one 
    for row in csvreader: 
        rows.append(row) 
  
    # get total number of rows 
    print("Total no. of rows: %d"%(csvreader.line_num))
    no_of_students = csvreader.line_num  

  
# printing the field names 
 
  
#  printing first 5 rows 
print('\nThe records are:\n') 
for row in rows: 
    # parsing each column of a row 
    for col in row: 
        print("%10s"%col), 
    print('\n') 



class Student:
	def __init__(self, list1):
		self.name = list1[0]
		self.roll_no = list1[1]
		self.GPA = list1[2]
		self.JEE_rank = list1[3]
		self.current_branch = list1[4]
		self.preference_branch = [list1[5], list1[6], list1[7]]
		self.status_of_branch = [list1[8], list1[9], list1[10]]

	def writeBack(self, list1):
		list1[0] = self.name
		list1[1] = self.roll_no
		list1[2] = self.GPA
		list1[3] = self.JEE_rank
		list1[4] = self.current_branch
		list1[5] = self.preference_branch[0]
		list1[6] = self.preference_branch[1]
		list1[7] = self.preference_branch[2]
		list1[8] = self.status_of_branch[0]
		list1[9] = self.status_of_branch[1]
		list1[10] = self.status_of_branch[2]

writeToReport = open("report.md",'w')


for i in range(0,no_of_students):
	currentStudent = Student(rows[i])
	
	writeToReport.write(currentStudent.name + " current branch is " + currentStudent.current_branch + "\n")
	for j in range(0,3):
		if currentStudent.status_of_branch[j] == 'yes':
			writeToReport.write(" - got into branch " + currentStudent.preference_branch[j] + "\n")

		elif currentStudent.status_of_branch[j] == 'none':
			writeToReport.write(" - Doesn't have preference " + str(j+1) + "\n")

		elif currentStudent.status_of_branch[j] == 'already got a preference':
			writeToReport.write(" - Already got a preference branch\n")

		else:
			writeToReport.write(" - Did not get into " + currentStudent.preference_branch[j] + " because " + currentStudent.status_of_branch[j] + "\n")

	writeToReport.write("\n")
